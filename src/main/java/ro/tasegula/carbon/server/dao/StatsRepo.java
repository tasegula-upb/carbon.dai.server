/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import ro.tasegula.carbon.server.model.StatsEntity;

import java.util.List;

public interface StatsRepo extends PagingAndSortingRepository<StatsEntity, Long> {
	/**
	 * @param id the USER id for which to retrieve ALL the STATS
	 * @return list of STATS
	 */
	List<StatsEntity> findByUserId(long id);

	StatsEntity findByUserIdAndQuestionId(long userId, long questionId);

	List<StatsEntity> findPageByUserId(long id, Pageable page);

}
