/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.util.DB.DbPages;

import java.util.List;

public interface SuiteRepo extends PagingAndSortingRepository<SuiteEntity, Long> {

	List<SuiteEntity> findByIsPublicTrue();

	List<SuiteEntity> findByCategoriesId(long id);

	List<SuiteEntity> findByCollectionsId(long id);

	int countByCategoriesId(long id);

	int countByCollectionsId(long id);

	List<SuiteEntity> findPageByCategoriesId(long id, Pageable page);

	List<SuiteEntity> findPageByCollectionsId(long id, Pageable page);

	@Query("SELECT x.id from SuiteEntity x INNER JOIN x.categories c where c.id = ?1")
	List<Long> findIdsByCategoriesId(long id);

	@Query("SELECT x.id from SuiteEntity x INNER JOIN x.collections c where c.id = ?1")
	List<Long> findIdsByCollectionsId(long id);

	default List<SuiteEntity> findFirstByCategoriesId(long id, int max) {
		return findPageByCategoriesId(id, DbPages.getMax(max));
	}

	default List<SuiteEntity> findFirstByCollectionsId(long id, int max) {
		return findPageByCollectionsId(id, DbPages.getMax(max));
	}

	default int countPublic() {
		return findByIsPublicTrue().size();
	}
}
