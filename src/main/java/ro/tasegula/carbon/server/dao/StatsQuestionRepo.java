/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.StatsQuestionEntity;

import java.util.List;

public interface StatsQuestionRepo extends CrudRepository<StatsQuestionEntity, Long> {
	/**
	 * @param statsId the ID of the STATS or which to retrieve ALL the given ANSWERS
	 * @return a list of the ANSWERS for that question
	 */
	List<StatsQuestionEntity> findByStatsId(long statsId);
}
