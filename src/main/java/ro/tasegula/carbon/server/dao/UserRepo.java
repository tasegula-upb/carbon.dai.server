/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.UserEntity;

public interface UserRepo extends CrudRepository<UserEntity, Long> {
	UserEntity findByGoogleId(String value);

	UserEntity findByGoogleEmail(String value);
}
