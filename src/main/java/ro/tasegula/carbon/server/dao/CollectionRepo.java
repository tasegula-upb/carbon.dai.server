/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.CollectionEntity;
import ro.tasegula.carbon.server.model.wrappers.SetWrapper;

import java.util.List;

public interface CollectionRepo extends CrudRepository<CollectionEntity, Long> {
	List<CollectionEntity> findByUserId(long id);

	@Query("SELECT new ro.tasegula.carbon.server.model.wrappers.SetWrapper(x.id, x.name) from CollectionEntity x")
	List<SetWrapper> findAllNames();
}
