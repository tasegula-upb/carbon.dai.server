/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.ImageEntity;

import java.util.List;

public interface ImageRepo extends CrudRepository<ImageEntity, Long> {
	List<ImageEntity> findByFilepath(String filepath);
}
