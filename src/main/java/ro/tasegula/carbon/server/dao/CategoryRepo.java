/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.CategoryEntity;
import ro.tasegula.carbon.server.model.wrappers.SetWrapper;

import java.util.List;

public interface CategoryRepo extends CrudRepository<CategoryEntity, Long> {
	CategoryEntity findByName(String name);

	@Query("SELECT new ro.tasegula.carbon.server.model.wrappers.SetWrapper(x.id, x.name) from CategoryEntity x")
	List<SetWrapper> findAllNames();

	default int countPublic() {
		return (int) count();
	}
}
