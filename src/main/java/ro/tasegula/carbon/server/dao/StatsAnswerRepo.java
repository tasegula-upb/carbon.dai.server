/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.StatsAnswerEntity;

import java.util.List;
import java.util.stream.Collectors;

public interface StatsAnswerRepo extends CrudRepository<StatsAnswerEntity, Long> {
	/**
	 * @param statsQuestionId the ID of the ANSWERED Question for which to retrieve the
	 *                        given ANSWERS at that time
	 * @return a list of the given ANSWERS
	 */
	List<StatsAnswerEntity> findByStatsQuestionId(long statsQuestionId);

	default List<String> findAnswerByStatsQuestionId(long statsQuestionId) {
		return findByStatsQuestionId(statsQuestionId)
				.stream()
				.map(StatsAnswerEntity::getAnswer)
				.collect(Collectors.toList());
	}
}
