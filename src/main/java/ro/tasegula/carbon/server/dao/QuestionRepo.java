/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ro.tasegula.carbon.server.model.QuestionEntity;
import ro.tasegula.carbon.server.util.DB.DbPages;

import java.util.List;

public interface QuestionRepo extends PagingAndSortingRepository<QuestionEntity, Long> {
	List<QuestionEntity> findBySuiteId(long suiteId);

	List<QuestionEntity> findPageBySuiteId(long suiteId, Pageable pageRequest);

	int countBySuiteId(long id);

	@Query("SELECT x.id from QuestionEntity x INNER JOIN x.suite s where s.id = ?1")
	List<Long> findIdsBySuiteId(long id);


	default List<QuestionEntity> findFirstBySuitesId(long suiteId, int max) {
		return findPageBySuiteId(suiteId, DbPages.getMax(max));
	}
}