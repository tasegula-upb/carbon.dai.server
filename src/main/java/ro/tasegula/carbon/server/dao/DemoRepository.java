package ro.tasegula.carbon.server.dao;

import org.springframework.data.repository.CrudRepository;
import ro.tasegula.carbon.server.model.DemoEntity;

public interface DemoRepository extends CrudRepository<DemoEntity, Long> {
}
