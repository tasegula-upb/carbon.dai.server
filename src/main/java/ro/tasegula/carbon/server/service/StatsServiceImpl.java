package ro.tasegula.carbon.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tasegula.carbon.server.dao.*;
import ro.tasegula.carbon.server.model.QuestionEntity;
import ro.tasegula.carbon.server.model.StatsAnswerEntity;
import ro.tasegula.carbon.server.model.StatsEntity;
import ro.tasegula.carbon.server.model.StatsQuestionEntity;
import ro.tasegula.carbon.server.model.wrappers.*;
import ro.tasegula.carbon.server.util.DB.DbPages;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class StatsServiceImpl implements StatsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatsServiceImpl.class);

	@Autowired
	StatsRepo statsRepo;

	@Autowired
	StatsQuestionRepo statsQuestionRepo;

	@Autowired
	StatsAnswerRepo statsAnswerRepo;

	@Autowired
	QuestionRepo questionRepo;

	@Autowired
	UserRepo userRepo;

	@Override
	public StatsQuestionWrapper findById(long id) {
		StatsEntity stats = statsRepo.findOne(id);
		QuestionEntity question = questionRepo.findOne(stats.getQuestionId());

		// get all the question stats
		List<StatsQuestionEntity> statsQuestionEntities = statsQuestionRepo.findByStatsId(stats.getId());
		List<StatsAnswerWrapper> answerList = new ArrayList<>(statsQuestionEntities.size());
		statsQuestionEntities.forEach(entity -> answerList.add(new StatsAnswerWrapper(entity)));

		// create the wrapper
		StatsQuestionWrapper statsQuestionWrapper = new StatsQuestionWrapper(stats, question);
		statsQuestionWrapper.setAnswerStats(answerList);

		return new StatsQuestionWrapper(stats, question);
	}

	@Override
	public StatsQuestionSimpleWrapper findSimpleById(long id) {
		StatsEntity stats = statsRepo.findOne(id);
		QuestionEntity question = questionRepo.findOne(stats.getQuestionId());

		return new StatsQuestionSimpleWrapper(stats, question);
	}

	@Override
	public StatsWrapper<StatsQuestionWrapper> findStatsByUserId(long id) {
		List<StatsEntity> statsList = statsRepo.findByUserId(id);
		List<StatsQuestionWrapper> questionWrappers = createQuestionWrapper(statsList);

		return new StatsWrapper<>(id, questionWrappers);
	}

	@Override
	public StatsWrapper<StatsQuestionSimpleWrapper> findSimpleStatsByUserId(long id) {
		List<StatsEntity> statsList = statsRepo.findByUserId(id);
		List<StatsQuestionSimpleWrapper> questionWrappers = createQuestionSimpleWrapper(statsList);

		return new StatsWrapper<>(id, questionWrappers);
	}

	@Override
	public StatsWrapper<StatsQuestionWrapper> findStatsPageByUserId(long id, int page) {
		List<StatsEntity> statsList = statsRepo.findPageByUserId(id, DbPages.getPage(page));
		List<StatsQuestionWrapper> questionWrappers = createQuestionWrapper(statsList);

		return new StatsWrapper<>(id, questionWrappers);
	}

	@Override
	public StatsWrapper<StatsQuestionSimpleWrapper> findSimpleStatsPageByUserId(long id, int page) {
		List<StatsEntity> statsList = statsRepo.findPageByUserId(id, DbPages.getPage(page));
		List<StatsQuestionSimpleWrapper> questionWrappers = createQuestionSimpleWrapper(statsList);

		return new StatsWrapper<>(id, questionWrappers);
	}

	@Override
	public void addStats(StatsPostWrapper stats) {
		addStats(stats.getUserId(), stats.makeSimple());
	}

	@Override
	public void addStats(long userId, StatsPostSimpleWrapper stats) {
		StatsEntity statsEntity = statsRepo.findByUserIdAndQuestionId(userId, stats.getQuestionId());
		if (statsEntity == null) {
			statsEntity = new StatsEntity();
			statsEntity.setQuestionId(stats.getQuestionId());
			statsEntity.setUser(userRepo.findOne(userId));

			statsEntity = statsRepo.save(statsEntity);
		}

		StatsQuestionEntity statsQuestionEntity = new StatsQuestionEntity();

		statsQuestionEntity.setStats(statsEntity);
		statsQuestionEntity.setAnsweredAt(stats.getAnsweredAt());
		statsQuestionEntity.setIsCorrect(stats.isCorrect());
		statsQuestionEntity.setRating(stats.getRating());

		statsQuestionEntity = statsQuestionRepo.save(statsQuestionEntity);

		for (int i = 0; i < stats.getAnswers().size(); i++) {
			StatsAnswerEntity answer = new StatsAnswerEntity();

			answer.setAnswer(stats.getAnswers().get(i));
			answer.setOrderInQuestion(i);
			answer.setStatsQuestion(statsQuestionEntity);

			statsAnswerRepo.save(answer);
		}
	}

	// ----------------------------------------------------------------------------
	// region PRIVATE HELPERS

	private List<StatsQuestionWrapper> createQuestionWrapper(List<StatsEntity> statsList) {
		int size = statsList.size();
		List<StatsQuestionWrapper> questionWrappers = new ArrayList<>(size);

		// convert to STATS QUESTION WRAPPER
		statsList.forEach(stats -> {
			// get the question entity
			QuestionEntity question = questionRepo.findOne(stats.getQuestionId());

			// get all the question stats
			List<StatsQuestionEntity> statsQuestionEntities = statsQuestionRepo.findByStatsId(stats.getId());
			List<StatsAnswerWrapper> answerList = new ArrayList<>(statsQuestionEntities.size());
			statsQuestionEntities.forEach(entity -> answerList.add(new StatsAnswerWrapper(entity)));

			// create the wrapper
			StatsQuestionWrapper statsQuestionWrapper = new StatsQuestionWrapper(stats, question);
			statsQuestionWrapper.setAnswerStats(answerList);

			// add the wrapper
			questionWrappers.add(statsQuestionWrapper);
		});

		return questionWrappers;
	}

	private List<StatsQuestionSimpleWrapper> createQuestionSimpleWrapper(List<StatsEntity> statsList) {
		int size = statsList.size();
		List<StatsQuestionSimpleWrapper> questionWrappers = new ArrayList<>(size);

		// convert to STATS QUESTION WRAPPER
		statsList.forEach(stats -> {
			// get the question entity
			QuestionEntity question = questionRepo.findOne(stats.getQuestionId());

			// create the wrapper
			StatsQuestionSimpleWrapper statsQuestionWrapper = new StatsQuestionSimpleWrapper(stats, question);

			// add the wrapper
			questionWrappers.add(statsQuestionWrapper);
		});

		return questionWrappers;
	}

	// endregion
	// ----------------------------------------------------------------------------
}
