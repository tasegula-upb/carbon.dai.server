package ro.tasegula.carbon.server.service;

import ro.tasegula.carbon.server.model.SuiteEntity;

import java.util.List;

public interface UserService {
	int countSuitesByUserId(long userId);

	int countQuestionsByUserId(long userId);

	List<SuiteEntity> findSuitesByUserId(long userId);

	List<SuiteEntity> findPrivateSuitesByUserId(long userId);
}