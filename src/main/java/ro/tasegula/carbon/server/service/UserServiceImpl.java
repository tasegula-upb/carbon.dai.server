package ro.tasegula.carbon.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tasegula.carbon.server.dao.CollectionRepo;
import ro.tasegula.carbon.server.model.CollectionEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private CollectionRepo collectionRepo;

	@Override
	public int countSuitesByUserId(long userId) {
		List<CollectionEntity> collections = collectionRepo.findByUserId(userId);
		AtomicInteger count = new AtomicInteger(0);

		collections.forEach(collection -> count.addAndGet(collection.getSuites().size()));
		return count.intValue();
	}

	@Override
	public int countQuestionsByUserId(long userId) {
		List<CollectionEntity> collections = collectionRepo.findByUserId(userId);
		List<SuiteEntity> suites = new ArrayList<>();
		AtomicInteger count = new AtomicInteger(0);

		collections.forEach(collection -> suites.addAll(collection.getSuites()));
		suites.forEach(suite -> count.addAndGet(suite.getQuestions().size()));

		return count.intValue();
	}

	@Override
	public List<SuiteEntity> findSuitesByUserId(long userId) {
		List<CollectionEntity> collections = collectionRepo.findByUserId(userId);

		return collections.stream()
				.flatMap(x -> x.getSuites().stream())
				.collect(Collectors.toList());
	}

	@Override
	public List<SuiteEntity> findPrivateSuitesByUserId(long userId) {
		List<CollectionEntity> collections = collectionRepo.findByUserId(userId);

		return collections.stream()
				.flatMap(x -> x.getSuites().stream())
				.filter(suite -> !suite.isPublic())
				.collect(Collectors.toList());
	}
}
