package ro.tasegula.carbon.server.service;

import ro.tasegula.carbon.server.model.wrappers.*;

public interface StatsService {
	StatsQuestionWrapper findById(long id);

	StatsQuestionSimpleWrapper findSimpleById(long id);

	StatsWrapper<StatsQuestionWrapper> findStatsByUserId(long id);

	StatsWrapper<StatsQuestionSimpleWrapper> findSimpleStatsByUserId(long id);

	StatsWrapper<StatsQuestionWrapper> findStatsPageByUserId(long id, int page);

	StatsWrapper<StatsQuestionSimpleWrapper> findSimpleStatsPageByUserId(long id, int page);

	void addStats(StatsPostWrapper stats);

	void addStats(long userId, StatsPostSimpleWrapper stats);
}
