/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.STATS)
public class StatsEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public StatsEntity() {
	}

	public StatsEntity(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.QUESTION_ID)
	private long questionId;

	// -------------------------------------------------------------------------
	// JOINs
	// -------------------------------------------------------------------------

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.USER_ID)
	@JsonBackReference
	private UserEntity user;

	@OneToMany(fetch = FetchType.LAZY,
			targetEntity = StatsQuestionEntity.class,
			mappedBy = "stats")
	@JsonManagedReference
	private List<StatsQuestionEntity> stats = new ArrayList<>();

	// ----------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ----------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	//endregion

	//region ENTITY ACCESSOR
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public List<StatsQuestionEntity> getStats() {
		return stats;
	}

	public void setStats(List<StatsQuestionEntity> stats) {
		this.stats = stats;
	}

	//endregion

	// ----------------------------------------------------------------------------
	// HELPERS
	// ----------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String USER_ID = "userId";
		public static final String QUESTION_ID = "questionId";
	}

	@Override
	public String toString() {
		return "{\"StatsEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"questionId\":\"" + questionId + "\""
				+ ", \"user\":" + Utils.EntityToId(user)
				+ ", \"stats\":" + Utils.CollectionToId(stats)
				+ "}}";
	}
}
