/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.*;
import ro.tasegula.carbon.server.util.stats.StatsData;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class StatsQuestionSimpleWrapper {
	protected long statsId;

	protected long suiteId;
	protected String suiteName;

	protected long questionId;
	protected String question;

	protected String answer;

	protected String imageUri;
	protected StatsData data;

	public StatsQuestionSimpleWrapper(StatsEntity statsEntity, QuestionEntity entity) {
		ImageEntity image = entity.getImage();
		SuiteEntity suite = entity.getSuite();

		this.statsId = statsEntity.getId();
		this.questionId = entity.getId();
		this.question = entity.getQuestion();
		this.imageUri = (image == null) ? "" : image.getUri();
		this.suiteId = suite.getId();
		this.suiteName = suite.getName();

		this.answer = entity.getAnswers().stream()
				.map(answerEntity -> answerEntity.getAnswer())
				.collect(Collectors.joining(","));

		int correct = 0;
		int wrong = 0;
		int rating[] = {0, 0, 0, 0, 0};

		List<StatsQuestionEntity> questionList = statsEntity.getStats();
		for (StatsQuestionEntity question : questionList) {
			int thisRating = question.getRating() - 1;
			if (thisRating >= 0) rating[thisRating]++;

			if (question.isCorrect()) correct++;
			else wrong++;
		}
		this.data = new StatsData(correct, wrong, rating);
	}

	public long getStatsId() {
		return statsId;
	}

	public void setStatsId(long statsId) {
		this.statsId = statsId;
	}

	public long getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(long suiteId) {
		this.suiteId = suiteId;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public StatsData getData() {
		return data;
	}

	public void setData(StatsData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "{\"StatsQuestionSimpleWrapper\":{"
				+ "\"statsId\":\"" + statsId + "\""
				+ ", \"questionId\":\"" + questionId + "\""
				+ ", \"question\":\"" + question + "\""
				+ ", \"suiteId\":\"" + suiteId + "\""
				+ ", \"suiteName\":\"" + suiteName + "\""
				+ ", \"imageUri\":\"" + imageUri + "\""
				+ ", \"data\":" + data
				+ "}}";
	}
}
