/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import java.util.Arrays;

public class DataWrapper<T> {

	T body;
	Object[] data;

	public DataWrapper() {
	}

	public DataWrapper(T body, Object[] data) {
		this.body = body;
		this.data = data;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	public Object[] getData() {
		return data;
	}

	public void setData(Object[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "{\"DataWrapper\":{"
				+ "\"body\":" + body
				+ ", \"data\":" + Arrays.toString(data)
				+ "}}";
	}
}
