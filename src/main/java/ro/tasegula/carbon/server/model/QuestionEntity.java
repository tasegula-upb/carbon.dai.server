/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import ro.tasegula.carbon.server.model.wrappers.AnswerWrapper;
import ro.tasegula.carbon.server.model.wrappers.QuestionWrapper;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.QUESTION)
public class QuestionEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public QuestionEntity() {
		suite = null;
		image = null;
	}

	public static QuestionEntity from(QuestionWrapper question) {
		if (question != null) {
			QuestionEntity entity = new QuestionEntity();
			SuiteEntity suite = new SuiteEntity();
			suite.setId(question.getSuiteId());

			entity.setSuite(suite);
			entity.setQuestion(question.getQuestion());
			entity.setHint(question.getHint());

			List<AnswerEntity> answers = new ArrayList<>(5);

			for (int i = 0; i < question.getAnswers().size(); i++) {
				AnswerEntity answer = AnswerEntity.from(question.getAnswers().get(i));
				answer.setOrderInQuestion(i + 1);
				answer.setQuestion(entity);
				answers.add(answer);
			}

			entity.setAnswers(answers);
			return entity;
		}
		return null;
	}

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.QUESTION)
	private String question;

	@Column(name = Fields.HINT)
	private String hint;


	@CreationTimestamp
	@Column(name = Fields.CREATED_AT)
	private Timestamp createdAt;

	// ------------------------------------------------------------------------
	// JOINs
	// ------------------------------------------------------------------------

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.SUITE_ID)
	@JsonBackReference
	private SuiteEntity suite;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.IMAGE_ID)
	private ImageEntity image;

	@OneToMany(cascade = CascadeType.PERSIST,
			fetch = FetchType.EAGER,
			targetEntity = AnswerEntity.class,
			mappedBy = "question")
	@JsonManagedReference
	private List<AnswerEntity> answers = new ArrayList<>();

	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	//endregion

	//region ENTITY ACCESSOR
	public SuiteEntity getSuite() {
		return suite;
	}

	public void setSuite(SuiteEntity suite) {
		this.suite = suite;
	}

	public ImageEntity getImage() {
		return image;
	}

	public void setImage(ImageEntity image) {
		this.image = image;
	}

	public List<AnswerEntity> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerEntity> answers) {
		this.answers = answers;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String QUESTION = "question";
		public static final String HINT = "hint";
		public static final String SUITE_ID = "suiteId";
		public static final String IMAGE_ID = "imageId";
		public static final String CREATED_AT = "createdAt";
	}

	@Override
	public String toString() {
		return "{\"QuestionEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"question\":\"" + question + "\""
				+ ", \"hint\":\"" + hint + "\""
				+ ", \"createdAt\":" + createdAt
				+ ", \"suite\":" + Utils.EntityToId(suite)
				+ ", \"image\":" + Utils.EntityToId(image)
				+ ", \"answers\":" + Utils.CollectionToId(answers)
				+ "}}";
	}
}
