/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.UserEntity;

@SuppressWarnings("unused")
public class UserWrapper {

	private long id;
	private String googleId;
	private String googleName;
	private String googleEmail;
	private String googleImage;

	private UserWrapper() {
	}

	public static UserWrapper from(UserEntity entity) {
		if (entity != null) {
			UserWrapper user = new UserWrapper();
			user.id = entity.getId();
			user.googleName = entity.getGoogleName();
			user.googleId = entity.getGoogleId();
			user.googleEmail = entity.getGoogleEmail();
			user.googleImage = entity.getGoogleImage();
			return user;
		}
		return null;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getGoogleName() {
		return googleName;
	}

	public void setGoogleName(String googleName) {
		this.googleName = googleName;
	}

	public String getGoogleEmail() {
		return googleEmail;
	}

	public void setGoogleEmail(String googleEmail) {
		this.googleEmail = googleEmail;
	}

	public String getGoogleImage() {
		return googleImage;
	}

	public void setGoogleImage(String googleImage) {
		this.googleImage = googleImage;
	}

	@Override
	public String toString() {
		return "{\"UserWrapper\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"googleId\":\"" + googleId + "\""
				+ ", \"googleName\":\"" + googleName + "\""
				+ ", \"googleEmail\":\"" + googleEmail + "\""
				+ ", \"googleImage\":\"" + googleImage + "\""
				+ "}}";
	}
}
