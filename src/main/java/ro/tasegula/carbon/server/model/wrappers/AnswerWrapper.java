/**
 * Project Carbon: created by Tase Gula.
 * <p/>
 * This file is part of the project's Android App Client.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.AnswerEntity;

@SuppressWarnings("unused")
public class AnswerWrapper {

	private String answer;
	private int pointX;
	private int pointY;

	public AnswerWrapper() {
	}

	public AnswerWrapper(AnswerEntity entity) {
		this.answer = entity.getAnswer();
		this.pointX = entity.getPointX();
		this.pointY = entity.getPointY();
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getPointX() {
		return pointX;
	}

	public void setPointX(int pointX) {
		this.pointX = pointX;
	}

	public int getPointY() {
		return pointY;
	}

	public void setPointY(int pointY) {
		this.pointY = pointY;
	}

	@Override
	public String toString() {
		return "{\"AnswerWrapper\":{"
				+ "\"answer\":\"" + answer + "\""
				+ ", \"pointX\":\"" + pointX + "\""
				+ ", \"pointY\":\"" + pointY + "\""
				+ "}}";
	}
}
