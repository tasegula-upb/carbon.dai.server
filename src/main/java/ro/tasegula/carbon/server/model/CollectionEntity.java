/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.COLLECTION)
public class CollectionEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public CollectionEntity() {
		user = null;
	}

	public CollectionEntity(long id) {
		this.id = id;
		user = null;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.NAME)
	private String name;

	// ------------------------------------------------------------------------
	// JOINs
	// ------------------------------------------------------------------------

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Fields.USER_ID)
	@JsonBackReference
	private UserEntity user;

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinTable(name = TableName.COLLECTION_HAS_SUITE,
			joinColumns = @JoinColumn(name = "collectionId", referencedColumnName = Fields.ID),
			inverseJoinColumns = @JoinColumn(name = "suiteId", referencedColumnName = SuiteEntity.Fields.ID))
	@JsonManagedReference
	private Collection<SuiteEntity> suites = new ArrayList<>();

	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	//endregion

	//region ENTITY ACCESSOR
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public Collection<SuiteEntity> getSuites() {
		return suites;
	}

	public void setSuites(Collection<SuiteEntity> suites) {
		this.suites = suites;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String USER_ID = "userId";
		public static final String NAME = "name";
	}

	@Override
	public String toString() {
		return "{\"CollectionEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ ", \"user\":" + Utils.EntityToId(user)
				+ ", \"suites\":" + suites
				+ "}}";
	}
}
