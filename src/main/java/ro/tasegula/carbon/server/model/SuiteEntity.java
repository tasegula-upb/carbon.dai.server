/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import ro.tasegula.carbon.server.model.wrappers.SuiteWrapper;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.SUITE)
public class SuiteEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public SuiteEntity() {
	}

	public SuiteEntity(long id) {
		this.id = id;
	}

	public static SuiteEntity from(SuiteWrapper wrapper) {
		if (wrapper != null) {
			SuiteEntity entity = new SuiteEntity();

			entity.setName(wrapper.getName());
			entity.setDescription(wrapper.getDescription());
			entity.setQuestions(null);
			entity.setImage(null);

			return entity;
		}
		return null;
	}

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.NAME)
	private String name;

	@Column(name = Fields.DESCRIPTION)
	private String description;

	@CreationTimestamp
	@Column(name = Fields.CREATED_AT)
	private Timestamp createdAt;

	@JsonIgnore
	@Column(name = Fields.IS_PUBLIC)
	private boolean isPublic;

	// -------------------------------------------------------------------------
	// JOINs
	// -------------------------------------------------------------------------

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.IMAGE_ID)
	private ImageEntity image;

	@OneToMany(fetch = FetchType.LAZY,
			targetEntity = QuestionEntity.class,
			mappedBy = "suite")
	@JsonManagedReference
	private Collection<QuestionEntity> questions = new ArrayList<>();

	@ManyToMany(mappedBy = "suites")
	@JsonBackReference
	private Collection<CategoryEntity> categories = new ArrayList<>();

	@ManyToMany(mappedBy = "suites")
	@JsonBackReference
	private Collection<CollectionEntity> collections = new ArrayList<>();

	// ----------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ----------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setIsPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	//endregion

	//region ENTITY ACCESSOR
	public ImageEntity getImage() {
		return image;
	}

	public void setImage(ImageEntity image) {
		this.image = image;
	}

	public Collection<QuestionEntity> getQuestions() {
		return questions;
	}

	public void setQuestions(Collection<QuestionEntity> questions) {
		this.questions = questions;
	}

	public Collection<CategoryEntity> getCategories() {
		return categories;
	}

	public void setCategories(Collection<CategoryEntity> categories) {
		this.categories = categories;
	}

	public Collection<CollectionEntity> getCollections() {
		return collections;
	}

	public void setCollections(Collection<CollectionEntity> collections) {
		this.collections = collections;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String DESCRIPTION = "description";
		public static final String IMAGE_ID = "imageId";
		public static final String CREATED_AT = "createdAt";
		public static final String IS_PUBLIC = "isPublic";
	}

	@Override
	public String toString() {
		return "{\"SuiteEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ ", \"description\":\"" + description + "\""
				+ ", \"createdAt\":" + createdAt
				+ ", \"isPublic\":\"" + isPublic + "\""
				+ ", \"image\":" + Utils.EntityToId(image)
				+ ", \"questions\":" + Utils.CollectionToId(questions)
				+ "}}";
	}
}
