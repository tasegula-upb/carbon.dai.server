/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.SuiteEntity;

import java.sql.Timestamp;

@SuppressWarnings("unused")
public class SuiteWrapper {

	private long id;

	private String name;
	private String description;

	private Timestamp createdAt;

	private String imageUri;

	private int noQuestions;

	public SuiteWrapper() {
	}

	public SuiteWrapper(SuiteEntity entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.description = entity.getDescription();
		this.createdAt = entity.getCreatedAt();

		this.imageUri = (entity.getImage() == null) ? null : entity.getImage().getUri();
	}

	public SuiteWrapper(SuiteEntity entity, int noQuestions) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.description = entity.getDescription();
		this.createdAt = entity.getCreatedAt();

		this.imageUri = entity.getImage().getUri();

		this.noQuestions = noQuestions;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public int getNoQuestions() {
		return noQuestions;
	}

	public void setNoQuestions(int noQuestions) {
		this.noQuestions = noQuestions;
	}

	@Override
	public String toString() {
		return "{\"SuiteWrapper\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ ", \"description\":\"" + description + "\""
				+ ", \"createdAt\":" + createdAt
				+ ", \"imageUri\":\"" + imageUri + "\""
				+ ", \"noQuestions\":\"" + noQuestions + "\""
				+ "}}";
	}
}
