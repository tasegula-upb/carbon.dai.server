/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.CategoryEntity;
import ro.tasegula.carbon.server.model.CollectionEntity;

@SuppressWarnings("unused")
public class SetWrapper {

	private long id;
	private String name;

	private int noSuites;

	public SetWrapper() {
	}

	public SetWrapper(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public SetWrapper(CategoryEntity entity, int noSuites) {
		this.id = entity.getId();
		this.name = entity.getName();

		this.noSuites = noSuites;
	}

	public SetWrapper(CollectionEntity entity, int noSuites) {
		this.id = entity.getId();
		this.name = entity.getName();

		this.noSuites = noSuites;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoSuites() {
		return noSuites;
	}

	public void setNoSuites(int noSuites) {
		this.noSuites = noSuites;
	}

	@Override
	public String toString() {
		return "{\"SetWrapper\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ ", \"noSuites\":\"" + noSuites + "\""
				+ "}}";
	}
}
