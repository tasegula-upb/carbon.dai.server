package ro.tasegula.carbon.server.model;

public interface AbstractEntityWithId {
	long getId();

	void setId(long id);
}
