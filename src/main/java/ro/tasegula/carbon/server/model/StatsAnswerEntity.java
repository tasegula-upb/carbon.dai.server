/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.STATS_ANSWER)
public class StatsAnswerEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public StatsAnswerEntity() {
		statsQuestion = null;
	}

	public StatsAnswerEntity(long id) {
		this.id = id;
		statsQuestion = null;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.ANSWER)
	private String answer;

	@Column(name = Fields.ORDER_IN_QUESTION)
	private int orderInQuestion;

	// ------------------------------------------------------------------------
	// JOINs
	// ------------------------------------------------------------------------

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.QUESTION_ID)
	@JsonBackReference
	private StatsQuestionEntity statsQuestion;

	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getOrderInQuestion() {
		return orderInQuestion;
	}

	public void setOrderInQuestion(int orderInQuestion) {
		this.orderInQuestion = orderInQuestion;
	}
	//endregion

	//region ENTITY ACCESSOR
	public StatsQuestionEntity getStatsQuestion() {
		return statsQuestion;
	}

	public void setStatsQuestion(StatsQuestionEntity statsQuestion) {
		this.statsQuestion = statsQuestion;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String QUESTION_ID = "statsQuestionId";
		public static final String ANSWER = "answer";
		public static final String ORDER_IN_QUESTION = "orderInQuestion";
	}

	@Override
	public String toString() {
		return "{\"StatsAnswerEntity36\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"answer\":\"" + answer + "\""
				+ ", \"orderInQuestion\":\"" + orderInQuestion + "\""
				+ ", \"statsQuestion\":" + Utils.EntityToId(statsQuestion)
				+ "}}";
	}
}
