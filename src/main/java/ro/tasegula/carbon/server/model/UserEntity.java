/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import ro.tasegula.carbon.server.model.wrappers.UserWrapper;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.USER)
public class UserEntity implements AbstractEntityWithId {

	public UserEntity() {
	}

	public UserEntity(long id) {
		this.id = id;
	}

	public static UserEntity from(UserWrapper entity) {
		if (entity != null) {
			UserEntity user = new UserEntity();
			user.setGoogleName(entity.getGoogleName());
			user.setGoogleEmail(entity.getGoogleEmail());
			user.setGoogleId(entity.getGoogleId());
			user.setGoogleImage(entity.getGoogleImage());
			return user;
		}
		return null;
	}

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.GOOGLE_ID)
	private String googleId;

	@Column(name = Fields.GOOGLE_NAME)
	private String googleName;

	@Column(name = Fields.GOOGLE_EMAIL)
	private String googleEmail;

	@JsonIgnore
	@Column(name = Fields.GOOGLE_IMAGE)
	private String googleImage;

	@CreationTimestamp
	@Column(name = Fields.CREATED_AT)
	private Timestamp createdAt;

	// ----------------------------------------------------------------------------
	// JOIN
	// ----------------------------------------------------------------------------

	@OneToMany(fetch = FetchType.LAZY,
			targetEntity = StatsEntity.class,
			mappedBy = "user")
	@JsonManagedReference
	private Collection<StatsEntity> stats = new ArrayList<>();

	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELDS ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getGoogleName() {
		return googleName;
	}

	public void setGoogleName(String googleName) {
		this.googleName = googleName;
	}

	public String getGoogleEmail() {
		return googleEmail;
	}

	public void setGoogleEmail(String googleEmail) {
		this.googleEmail = googleEmail;
	}

	public String getGoogleImage() {
		return googleImage;
	}

	public void setGoogleImage(String googleImage) {
		this.googleImage = googleImage;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	//endregion

	//region ENTITY ACCESSOR
	public Collection<StatsEntity> getStats() {
		return stats;
	}

	public void setStats(Collection<StatsEntity> stats) {
		this.stats = stats;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String GOOGLE_ID = "googleId";
		public static final String GOOGLE_NAME = "googleName";
		public static final String GOOGLE_EMAIL = "googleEmail";
		public static final String GOOGLE_IMAGE = "googleImage";
		public static final String CREATED_AT = "createdAt";
	}

	@Override
	public String toString() {
		return "{\"UserEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"googleId\":\"" + googleId + "\""
				+ ", \"googleName\":\"" + googleName + "\""
				+ ", \"googleEmail\":\"" + googleEmail + "\""
				+ ", \"googleImage\":\"" + googleImage + "\""
				+ ", \"createdAt\":" + createdAt
				+ ", \"stats\":" + Utils.CollectionToId(stats)
				+ "}}";
	}
}
