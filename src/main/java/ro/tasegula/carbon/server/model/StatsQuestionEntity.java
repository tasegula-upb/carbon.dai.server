/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.STATS_QUESTION)
public class StatsQuestionEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	public StatsQuestionEntity() {
	}

	public StatsQuestionEntity(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.IS_CORRECT)
	private boolean isCorrect;

	@Min(0)
	@Max(5)
	@Column(name = Fields.RATING)
	private int rating;

	@CreationTimestamp
	@Column(name = Fields.DATE)
	private Timestamp answeredAt;

	// -------------------------------------------------------------------------
	// JOINs
	// -------------------------------------------------------------------------

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.STATS_ID)
	@JsonBackReference
	private StatsEntity stats;

	@OneToMany(cascade = CascadeType.PERSIST,
			fetch = FetchType.EAGER,
			targetEntity = StatsAnswerEntity.class,
			mappedBy = "statsQuestion")
	@JsonManagedReference
	private List<StatsAnswerEntity> statsAnswers = new ArrayList<>();

	// ----------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ----------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setIsCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}
	//endregion

	//region ENTITY ACCESSOR
	public StatsEntity getStats() {
		return stats;
	}

	public void setStats(StatsEntity stats) {
		this.stats = stats;
	}

	public List<StatsAnswerEntity> getStatsAnswers() {
		return statsAnswers;
	}

	public void setStatsAnswers(List<StatsAnswerEntity> statsAnswers) {
		this.statsAnswers = statsAnswers;
	}
	//endregion

	// ----------------------------------------------------------------------------
	// HELPERS
	// ----------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String STATS_ID = "statsId";
		public static final String DATE = "answeredAt";
		public static final String IS_CORRECT = "isCorrect";
		public static final String RATING = "rating";
	}

	@Override
	public String toString() {
		return "{\"StatsQuestionEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"isCorrect\":\"" + isCorrect + "\""
				+ ", \"rating\":\"" + rating + "\""
				+ ", \"answeredAt\":" + answeredAt
				+ ", \"stats\":" + Utils.EntityToId(stats)
				+ ", \"statsAnswers\":" + Utils.CollectionToId(statsAnswers)
				+ "}}";
	}
}
