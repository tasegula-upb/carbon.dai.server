/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

public class MediaWrapper<T> {

	T body;
	MultipartFile file;
	String[] data;

	public MediaWrapper() {
	}

	public MediaWrapper(T body, MultipartFile file, String[] data) {
		this.body = body;
		this.file = file;
		this.data = data;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "{\"MediaWrapper\":{"
				+ "\"body\":" + body
				+ ", \"file\":" + file
				+ ", \"data\":" + Arrays.toString(data)
				+ "}}";
	}
}
