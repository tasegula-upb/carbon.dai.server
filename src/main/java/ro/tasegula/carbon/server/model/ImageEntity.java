/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import ro.tasegula.carbon.server.util.DB.TableName;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.IMAGE)
public class ImageEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.FILENAME)
	private String filename;

	@Column(name = Fields.FILEPATH)
	private String filepath;

	@Column(name = Fields.SIZE)
	private double size;

	// ------------------------------------------------------------------------
	//region FIELD ACCESSOR

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public String getUri() {
		return filepath + "/" + filename;
	}

	//endregion
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String FILENAME = "filename";
		public static final String FILEPATH = "filepath";
		public static final String SIZE = "size";
	}

	@Override
	public String toString() {
		return "{\"ImageEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"filename\":\"" + filename + "\""
				+ ", \"filepath\":\"" + filepath + "\""
				+ ", \"size\":\"" + size + "\""
				+ "}}";
	}
}
