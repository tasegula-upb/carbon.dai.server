/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

public class DatabaseStatsWrapper {

	private int noSuites, noQuestions;
	private int noPublicSuites, noPulicCategories;

	public int getNoSuites() {
		return noSuites;
	}

	public void setNoSuites(int noSuites) {
		this.noSuites = noSuites;
	}

	public int getNoQuestions() {
		return noQuestions;
	}

	public void setNoQuestions(int noQuestions) {
		this.noQuestions = noQuestions;
	}

	public int getNoPublicSuites() {
		return noPublicSuites;
	}

	public void setNoPublicSuites(int noPublicSuites) {
		this.noPublicSuites = noPublicSuites;
	}

	public int getNoPulicCategories() {
		return noPulicCategories;
	}

	public void setNoPulicCategories(int noPulicCategories) {
		this.noPulicCategories = noPulicCategories;
	}

	@Override
	public String toString() {
		return "{\"DatabaseStatsWrapper\":{"
				+ "\"noSuites\":\"" + noSuites + "\""
				+ ", \"noQuestions\":\"" + noQuestions + "\""
				+ ", \"noPublicSuites\":\"" + noPublicSuites + "\""
				+ ", \"noPulicCategories\":\"" + noPulicCategories + "\""
				+ "}}";
	}
}
