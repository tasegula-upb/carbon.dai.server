/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.StatsAnswerEntity;
import ro.tasegula.carbon.server.model.StatsQuestionEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
/**
 * StatsAnswerWrapper is a wrapper over Stats Question Entity,
 * with Stringify over the List of Stats Answer Entity
 */
public class StatsAnswerWrapper {

	private Timestamp answeredAt;
	private boolean isCorrect;
	private int rating;

	List<String> answers = new ArrayList<>();

	public StatsAnswerWrapper(StatsQuestionEntity entity) {
		this.answeredAt = entity.getAnsweredAt();
		this.isCorrect = entity.isCorrect();
		this.rating = entity.getRating();

		List<StatsAnswerEntity> answerEntities = entity.getStatsAnswers();
		answerEntities.forEach(answer -> this.answers.add(answer.getAnswer()));
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setIsCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	@Override
	public String toString() {
		return "{\"StatsAnswerWrapper\":{"
				+ "\"answeredAt\":" + answeredAt
				+ ", \"isCorrect\":\"" + isCorrect + "\""
				+ ", \"rating\":\"" + rating + "\""
				+ ", \"answers\":" + answers
				+ "}}";
	}
}
