/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import ro.tasegula.carbon.server.util.DB.TableName;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import static ro.tasegula.carbon.server.util.DB.TableName.CATEGORY_HAS_SUITE;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.CATEGORY)
public class CategoryEntity implements AbstractEntityWithId {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.NAME)
	private String name;

	// ------------------------------------------------------------------------
	// JOINs
	// ------------------------------------------------------------------------

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinTable(name = CATEGORY_HAS_SUITE,
			joinColumns = @JoinColumn(name = "categoryId", referencedColumnName = Fields.ID),
			inverseJoinColumns = @JoinColumn(name = "suiteId", referencedColumnName = SuiteEntity.Fields.ID))
	@JsonManagedReference
	private Collection<SuiteEntity> suites = new ArrayList<>();


	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	//endregion

	//region ENTITY ACCESSOR
	public Collection<SuiteEntity> getSuites() {
		return suites;
	}

	public void setSuites(Collection<SuiteEntity> suites) {
		this.suites = suites;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String NAME = "name";
	}

	@Override
	public String toString() {
		return "{\"CategoryEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ ", \"suites\":" + suites
				+ "}}";
	}
}
