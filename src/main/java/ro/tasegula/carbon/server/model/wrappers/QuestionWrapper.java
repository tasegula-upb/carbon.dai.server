/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.AnswerEntity;
import ro.tasegula.carbon.server.model.QuestionEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused")
public class QuestionWrapper {
	private long id;

	private String question;
	private String hint;
	private List<AnswerWrapper> answers = new ArrayList<>();

	private Timestamp createdAt;

	private long suiteId;
	private String imageUri;

	public QuestionWrapper() {
	}

	public QuestionWrapper(QuestionEntity entity) {
		this.id = entity.getId();
		this.question = entity.getQuestion();
		this.hint = entity.getHint();
		this.createdAt = entity.getCreatedAt();
		this.imageUri = entity.getImage() == null ? null : entity.getImage().getUri();
		this.suiteId = entity.getSuite().getId();

		Collection<AnswerEntity> answerEntities = entity.getAnswers();
		answerEntities.forEach(answer -> this.answers.add(new AnswerWrapper(answer)));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public long getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(long suiteId) {
		this.suiteId = suiteId;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public List<AnswerWrapper> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerWrapper> answers) {
		this.answers = answers;
	}

	@Override
	public String toString() {
		return "{\"QuestionWrapper\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"question\":\"" + question + "\""
				+ ", \"hint\":\"" + hint + "\""
				+ ", \"answers\":" + answers
				+ ", \"createdAt\":" + createdAt
				+ ", \"suiteId\":\"" + suiteId + "\""
				+ ", \"imageUri\":\"" + imageUri + "\""
				+ "}}";
	}
}
