/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import java.sql.Timestamp;
import java.util.List;

public class StatsPostSimpleWrapper {

	private long questionId;
	private int rating;
	private boolean correct;
	private List<String> answers;
	private Timestamp answeredAt;

	public StatsPostSimpleWrapper() {
	}

	public StatsPostSimpleWrapper(long questionId, int rating, boolean correct, List<String> answers, Timestamp answeredAt) {
		this.questionId = questionId;
		this.correct = correct;
		this.rating = rating;
		this.answers = answers;
		this.answeredAt = answeredAt;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public Timestamp getAnsweredAt() {
		return answeredAt;
	}

	public void setAnsweredAt(Timestamp answeredAt) {
		this.answeredAt = answeredAt;
	}

	@Override
	public String toString() {
		return "{\"StatsPostSimpleWrapper\":{"
				+ "\"questionId\":\"" + questionId + "\""
				+ ", \"correct\":\"" + correct + "\""
				+ ", \"rating\":\"" + rating + "\""
				+ ", \"answers\":" + answers
				+ ", \"answeredAt\":" + answeredAt
				+ "}}";
	}
}
