/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import ro.tasegula.carbon.server.model.QuestionEntity;
import ro.tasegula.carbon.server.model.StatsEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
/**
 * StatsQuestionWrapper is a wrapper over Stats Entity
 * for a question
 */
public class StatsQuestionWrapper extends StatsQuestionSimpleWrapper {

	private List<StatsAnswerWrapper> answerStats = new ArrayList<>();
	private Timestamp createdAt;

	public StatsQuestionWrapper(StatsEntity statsEntity, QuestionEntity questionEntity) {
		super(statsEntity, questionEntity);
		this.createdAt = questionEntity.getCreatedAt();
	}

	public List<StatsAnswerWrapper> getAnswerStats() {
		return answerStats;
	}

	public void setAnswerStats(List<StatsAnswerWrapper> answerStats) {
		this.answerStats = answerStats;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "{\"StatsQuestionWrapper\":{"
				+ "\"answerStats\":" + answerStats
				+ ", \"createdAt\":" + createdAt
				+ "}}";
	}
}
