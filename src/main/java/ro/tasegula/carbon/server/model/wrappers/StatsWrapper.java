/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model.wrappers;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
/**
 * StatsWrapper is a wrapper over Stats Entity
 * for a user
 */
public class StatsWrapper<T> {

	private long userId;
	private List<T> stats = new ArrayList<>();

	public StatsWrapper(long userId, List<T> stats) {
		this.userId = userId;
		this.stats = stats;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<T> getStats() {
		return stats;
	}

	public void setStats(List<T> stats) {
		this.stats = stats;
	}

	@Override
	public String toString() {
		return "{\"StatsWrapper\":{"
				+ "\"userId\":\"" + userId + "\""
				+ ", \"stats\":" + stats
				+ "}}";
	}
}
