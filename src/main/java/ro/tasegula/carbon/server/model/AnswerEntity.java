/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import ro.tasegula.carbon.server.model.wrappers.AnswerWrapper;
import ro.tasegula.carbon.server.util.DB.TableName;
import ro.tasegula.carbon.server.util.Utils;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = TableName.ANSWER)
public class AnswerEntity implements AbstractEntityWithId {

	public AnswerEntity() {
		question = null;
	}

	public AnswerEntity(long id) {
		this.id = id;
		question = null;
	}

	public static AnswerEntity from(AnswerWrapper wrapper) {
		if (wrapper != null) {
			AnswerEntity answer = new AnswerEntity();
			answer.setAnswer(wrapper.getAnswer());
			answer.setPointX(wrapper.getPointX());
			answer.setPointY(wrapper.getPointY());
			return answer;
		}
		return null;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.ANSWER)
	private String answer;

	@Column(name = Fields.ORDER_IN_QUESTION)
	private int orderInQuestion;

	@Column(name = Fields.POINTX)
	private int pointX;

	@Column(name = Fields.POINTY)
	private int pointY;

	// ------------------------------------------------------------------------
	// JOINs
	// ------------------------------------------------------------------------

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = Fields.QUESTION_ID)
	@JsonBackReference
	private QuestionEntity question;

	// ------------------------------------------------------------------------
	// GETTERS AND SETTERS
	// ------------------------------------------------------------------------

	//region FIELD ACCESSOR
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getOrderInQuestion() {
		return orderInQuestion;
	}

	public void setOrderInQuestion(int orderInQuestion) {
		this.orderInQuestion = orderInQuestion;
	}

	public int getPointX() {
		return pointX;
	}

	public void setPointX(int pointX) {
		this.pointX = pointX;
	}

	public int getPointY() {
		return pointY;
	}

	public void setPointY(int pointY) {
		this.pointY = pointY;
	}
	//endregion

	//region ENTITY ACCESSOR
	public QuestionEntity getQuestion() {
		return question;
	}

	public void setQuestion(QuestionEntity question) {
		this.question = question;
	}
	//endregion

	// ------------------------------------------------------------------------
	// HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String QUESTION_ID = "questionId";
		public static final String ANSWER = "answer";
		public static final String ORDER_IN_QUESTION = "orderInQuestion";
		public static final String POINTX = "pointX";
		public static final String POINTY = "pointY";
	}

	@Override
	public String toString() {
		return "{\"AnswerEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"answer\":\"" + answer + "\""
				+ ", \"orderInQuestion\":\"" + orderInQuestion + "\""
				+ ", \"pointX\":\"" + pointX + "\""
				+ ", \"pointY\":\"" + pointY + "\""
				+ ", \"question\":" + Utils.EntityToId(question)
				+ "}}";
	}
}
