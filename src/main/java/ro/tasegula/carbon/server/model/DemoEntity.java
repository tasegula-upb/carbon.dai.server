package ro.tasegula.carbon.server.model;

import javax.persistence.*;

/**
 * Created by tase9 on 2017-05-03.
 */
@Entity
@Table(name = "Demo")
public class DemoEntity implements AbstractEntityWithId {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = Fields.ID)
	private long id;

	@Column(name = Fields.NAME)
	private String name;

	// ------------------------------------------------------------------------
	//region FIELD ACCESSOR
	// ------------------------------------------------------------------------

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	//endregion

	// ------------------------------------------------------------------------
	// region HELPERS
	// ------------------------------------------------------------------------

	public static final class Fields {
		public static final String ID = "id";
		public static final String NAME = "name";
	}
	// endregion

	@Override
	public String toString() {
		return "{\"DemoEntity\":{"
				+ "\"id\":\"" + id + "\""
				+ ", \"name\":\"" + name + "\""
				+ "}}";
	}
}
