/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util.DB;

public class TableName {
	public static final String CATEGORY = "category";
	public static final String CATEGORY_HAS_SUITE = "category_has_suite";
	public static final String COLLECTION = "collection";
	public static final String COLLECTION_HAS_SUITE = "collection_has_suite";
	public static final String SUITE = "suite";
	public static final String QUESTION = "question";
	public static final String ANSWER = "answer";
	public static final String IMAGE = "image";
	public static final String USER = "user";
	public static final String STATS = "stats";
	public static final String STATS_QUESTION = "stats_question";
	public static final String STATS_ANSWER = "stats_answer";
}
