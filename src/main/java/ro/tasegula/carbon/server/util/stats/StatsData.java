/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util.stats;

public class StatsData {

	public int correct = 0;
	public int wrong = 0;
	public int[] rating = {0, 0, 0, 0, 0};

	public float correctness = 0.0f;
	public float ratingFull = 0.0f;

	public StatsData() {
	}

	public StatsData(int correct, int wrong, int[] rating) {
		this.correct = correct;
		this.wrong = wrong;
		this.rating = rating;
		make();
	}

	public int getCorrect() {
		return correct;
	}

	public void setCorrect(int correct) {
		this.correct = correct;
	}

	public int getWrong() {
		return wrong;
	}

	public void setWrong(int wrong) {
		this.wrong = wrong;
	}

	public int[] getRating() {
		return rating;
	}

	public void setRating(int[] rating) {
		this.rating = rating;
	}

	public void make() {
		correctness = (wrong == 0) ? 100
				: (correct == 0) ? 0
				: (correct * 1.0f / (correct + wrong) * 100);

		int size = 0;
		ratingFull = 0;
		for (int i = 0; i < 5; i++) {
			size += rating[i];
			ratingFull += (i + 1) * rating[i];
		}
		if (size > 0)
			ratingFull /= size;
	}
}
