/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util.response;

@SuppressWarnings("unused")
public class Status {
	private final int code;
	private String message;
	private Object data[] = null;

	public Status(int code, String message) {
		this.code = code;
		this.message = message;
	}

	// -------------------------------------------------------------------------
	// BASIC STATIC OBJECTS
	// -------------------------------------------------------------------------

	public static final Status SUCCESS = new Status(1, "SUCCESS");
	public static final Status ERROR = new Status(0, "ERROR");

	// -------------------------------------------------------------------------
	// ACCESSORs
	// -------------------------------------------------------------------------

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public Status setMessage(String message) {
		this.message = message;
		return this;
	}

	public Object[] getData() {
		return data;
	}

	public Status setData(Object... data) {
		this.data = data;
		return this;
	}

// -------------------------------------------------------------------------
	// MODIFIERS
	// -------------------------------------------------------------------------

	public Status addMessage(String message) {
		return new Status(this.code, String.format("[%s] %s", this.message, message));
	}

	// ----------------------------------------------------------------------------
	// TESTING
	// ----------------------------------------------------------------------------

	public boolean isSuccess() {
		return (code == 1);
	}
}
