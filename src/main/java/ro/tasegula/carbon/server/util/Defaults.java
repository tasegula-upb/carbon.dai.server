/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util;

public class Defaults {

	public static final class Collection {
		public static final String NAME = "My Collection";

		public static String getName(String name) {
			return String.format("%s's Collection", name.split("[- ']"));
		}
	}

	public static final class Suite {
		public static final String NAME = "Quick Notes";
		public static final int IMAGE_ID = 697;
	}
}
