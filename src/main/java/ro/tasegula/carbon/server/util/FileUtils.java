/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

	public static long saveFile(MultipartFile file, String filepath, String filename) {
		if (!Files.isDirectory(Paths.get(filepath))) {
			try {
				Files.createDirectory(Paths.get(filepath));
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
				return -1;
			}
		}

		if (!file.isEmpty()) {
			String path = filepath + "/" + filename;
			return resolveFile(file, path);
		} else {
			LOGGER.error("FILE EMPTY: " + file);
			return -1;
		}
	}

	private static long resolveFile(MultipartFile file, String path) {
		try {
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream =
					new BufferedOutputStream(new FileOutputStream(new File(path)));
			stream.write(bytes);
			stream.close();

			String message = String.format("You successfully uploaded %s", path);
			LOGGER.info(message);
			return file.getSize();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return -1;
		}
	}

	public static String mailFormat(String email) {
		return email.replaceAll("\\.", "").replace('@', '_');
	}

	public static String mailPath(String initialPath, String email) {
		return initialPath + "/users/" + email.replaceAll("\\.", "").replace('@', '_');
	}

	public static String imagePath(String initialPath, String folder) {
		return initialPath + "/users/" + folder;
	}

	public static String userFolder(String email) {
		return email.replaceAll("@", "_").replaceAll("\\.", "");
	}

	public static String getExtension(String filename) {
		String toks[] = filename.split("\\.");
		return toks[toks.length - 1];
	}
}
