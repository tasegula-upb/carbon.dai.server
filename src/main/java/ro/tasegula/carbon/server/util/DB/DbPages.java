/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util.DB;

import org.springframework.data.domain.PageRequest;

public class DbPages {

	private static final int FIRST_PAGE = 0;
	private static final int PAGE_SIZE = 50;

	private static final int PAGE_SIZE_QUESTION = 50;
	private static final int PAGE_SIZE_STATS = 5;

	public static PageRequest getMax(int max) {
		return new PageRequest(0, max);
	}

	public static PageRequest firstPage(int page) {
		return new PageRequest(FIRST_PAGE, PAGE_SIZE);
	}

	public static PageRequest getPage(int page) {
		return new PageRequest(page, PAGE_SIZE);
	}

	public static PageRequest getQuestionPage(int page) {
		return new PageRequest(page, PAGE_SIZE_QUESTION);
	}

	public static PageRequest getSizePage(int page) {
		return new PageRequest(page, PAGE_SIZE_STATS);
	}
}
