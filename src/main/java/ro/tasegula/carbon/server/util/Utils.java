/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.util;

import com.google.common.collect.ImmutableList;
import ro.tasegula.carbon.server.model.AbstractEntityWithId;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

	public static <T extends AbstractEntityWithId> List<Long> CollectionToId(Collection<T> list) {
		if (list == null) return ImmutableList.of();
		return list.stream()
				.map(AbstractEntityWithId::getId)
				.collect(Collectors.toList());
	}

	public static <T extends AbstractEntityWithId> long EntityToId(T entity) {
		if (entity == null) return -1;
		return entity.getId();
	}

	public static boolean isStringEmtpy(String value) {
		return (value == null) || (value.isEmpty());
	}
}
