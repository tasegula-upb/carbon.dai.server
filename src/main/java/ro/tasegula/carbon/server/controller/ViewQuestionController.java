/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.tasegula.carbon.server.dao.*;
import ro.tasegula.carbon.server.model.AnswerEntity;
import ro.tasegula.carbon.server.model.ImageEntity;
import ro.tasegula.carbon.server.model.QuestionEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.model.wrappers.QuestionWrapper;
import ro.tasegula.carbon.server.util.FileUtils;
import ro.tasegula.carbon.server.util.Utils;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/question")
public class ViewQuestionController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewQuestionController.class);

	@Value("${image.path}")
	private String imagePath;

	@Autowired
	UserRepo userRepo;

	@Autowired
	QuestionRepo questionRepo;

	@Autowired
	SuiteRepo suiteRepo;

	@Autowired
	AnswerRepo answerRepo;

	@Autowired
	ImageRepo imageRepo;

	// ----------------------------------------------------------------------------
	// GET
	// ----------------------------------------------------------------------------

	/**
	 * @param id the id of the QUESTION
	 * @return the QUESTION from the DB
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	QuestionWrapper getQuestion(
			@PathVariable("id") long id
	) {
		QuestionWrapper response = new QuestionWrapper(questionRepo.findOne(id));
		return response;
	}

	// ----------------------------------------------------------------------------
	// POST
	// ----------------------------------------------------------------------------

	/**
	 * Adds a QUESTION in DB
	 *
	 * @param question the question to be added
	 * @return the object added in DB
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	QuestionWrapper postQuestion(
			@RequestBody QuestionWrapper question
	) {
		QuestionEntity entity = QuestionEntity.from(question);

		entity = constructQuestion(entity);
		return new QuestionWrapper(entity);
	}

	/**
	 * Adds a QUESTION in DB
	 *
	 * @param questionJson question data in String format
	 * @param file         the image file
	 * @param userId       the user id for which the suite is added
	 * @return the object added in DB
	 */
	@PostMapping(value = "/image",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	QuestionWrapper postQuestionWithImage(
			@RequestParam("body") String questionJson,
			@RequestParam("file") MultipartFile file,
			@RequestParam(value = "userId") long userId
	) {
		String userFolder = FileUtils.userFolder(userRepo.findOne(userId).getGoogleEmail());

		// convert object
		ObjectMapper mapper = new ObjectMapper();
		QuestionWrapper question;
		try {
			question = mapper.readValue(questionJson, QuestionWrapper.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		// create entity
		QuestionEntity entity = QuestionEntity.from(question);

		// construct image entity
		if (!Utils.isStringEmtpy(question.getImageUri())) {
			Timestamp time = new Timestamp((new Date()).getTime());

			String path = FileUtils.imagePath(imagePath, userFolder);
			String name = userFolder + "_" + time.getTime() + "." + FileUtils.getExtension(file.getOriginalFilename());
			long size = FileUtils.saveFile(file, path, name);

			ImageEntity image = constructImage(userFolder, name, size);
			entity.setImage(image);
		}

		// add to DB and return
		entity = constructQuestion(entity);
		return new QuestionWrapper(entity);
	}

	// ----------------------------------------------------------------------------
	// HELPERS
	// ----------------------------------------------------------------------------

	private ImageEntity constructImage(String filepath, String filename, long size) {
		ImageEntity image = new ImageEntity();

		image.setFilepath("/users/" + filepath);
		image.setFilename(filename);
		image.setSize(size);

		return imageRepo.save(image);
	}

	private QuestionEntity constructQuestion(QuestionEntity entity) {
		SuiteEntity suite = suiteRepo.findOne(entity.getSuite().getId());
		suite.getQuestions().add(entity);
		entity.setSuite(suite);

		questionRepo.save(entity);

		// add answers
		List<AnswerEntity> answers = entity.getAnswers();
		int size = answers.size();

		for (int i = 0; i < size; i++) {
			AnswerEntity answer = answers.get(i);
			answer.setQuestion(entity);
			answer = answerRepo.save(answer);
			answers.add(answer);
		}

		entity.setAnswers(answers);

		return entity;
	}
}
