/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tasegula.carbon.server.dao.CategoryRepo;
import ro.tasegula.carbon.server.dao.SuiteRepo;
import ro.tasegula.carbon.server.model.CategoryEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.model.wrappers.SetWrapper;
import ro.tasegula.carbon.server.model.wrappers.SuiteWrapper;
import ro.tasegula.carbon.server.util.DB.DbPages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/category")
public class ViewCategoryController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewCategoryController.class);

	@Autowired
	CategoryRepo categoryRepo;

	@Autowired
	SuiteRepo suiteService;

	/**
	 * @return all the CATEGORIES in the DB
	 */
	@GetMapping()
	public @ResponseBody
	Collection<SetWrapper> getCategories() {
		List<SetWrapper> response = categoryRepo.findAllNames();
		response.forEach(entity -> entity.setNoSuites(suiteService.countByCategoriesId(entity.getId())));
		return response;
	}

	/**
	 * @param id the id of the CATEGORY
	 * @return the CATEGORY from the DB
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	SetWrapper getCategory(
			@PathVariable("id") long id
	) {
		CategoryEntity response = categoryRepo.findOne(id);
		int noSuites = suiteService.countByCategoriesId(id);
		return new SetWrapper(response, noSuites);
	}

	/**
	 * @param id the id of the CATEGORY for which to retrieve the SUITES
	 * @return a list of all SUITES for the given CATEGORY
	 */
	@GetMapping(value = "/{id}/show")
	public @ResponseBody
	Collection<SuiteWrapper> getMaxSuites(
			@PathVariable("id") long id
	) {
		Collection<SuiteEntity> entityList;
		entityList = suiteService.findByCategoriesId(id);

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new SuiteWrapper(entity)));

		return wrappedList;
	}

	/**
	 * @param id  the id of the CATEGORY for which to retrieve the SUITES
	 * @param max the max number of SUITES to be retrieved
	 * @return max number of SUITES in the given CATEGORY
	 */
	@GetMapping(value = "/{id}/show",
			params = {"max"})
	public @ResponseBody
	Collection<SuiteWrapper> getMaxSuites(
			@PathVariable("id") long id,
			@RequestParam(value = "max") int max
	) {
		Collection<SuiteEntity> entityList;
		entityList = suiteService.findFirstByCategoriesId(id, max);

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new SuiteWrapper(entity)));

		return wrappedList;
	}

	/**
	 * @param id   the id of the CATEGORY for which to retrieve the SUITES
	 * @param page the page of SUITES to be retrieved
	 * @return a list of SUITES in the given page of the CATEGORY
	 */
	@GetMapping(value = "/{id}/show",
			params = {"page"})
	public @ResponseBody
	Collection<SuiteWrapper> getPageSuites(
			@PathVariable("id") long id,
			@RequestParam(value = "page") int page
	) {
		Collection<SuiteEntity> entityList;
		entityList = suiteService.findPageByCategoriesId(id, DbPages.getPage(page));

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new SuiteWrapper(entity)));

		return wrappedList;
	}
}
