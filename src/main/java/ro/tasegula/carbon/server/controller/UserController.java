/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tasegula.carbon.server.dao.*;
import ro.tasegula.carbon.server.model.CollectionEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.model.UserEntity;
import ro.tasegula.carbon.server.model.wrappers.*;
import ro.tasegula.carbon.server.service.StatsService;
import ro.tasegula.carbon.server.service.UserService;
import ro.tasegula.carbon.server.util.FileUtils;
import ro.tasegula.carbon.server.util.response.Status;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Value("${image.path}")
	private String imagePath;

	@Autowired
	UserService userService;

	@Autowired
	UserRepo userRepo;

	@Autowired
	CollectionRepo collectionRepo;

	@Autowired
	SuiteRepo suiteRepo;

	@Autowired
	QuestionRepo questionRepo;

	/**
	 * @param id the id of the USER
	 * @return the USER from the DB
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	UserWrapper getUser(
			@PathVariable("id") long id
	) {
		return UserWrapper.from(userRepo.findOne(id));
	}

	/**
	 * @param user basic user information
	 * @return the created user's information or the user's information from the db
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	UserWrapper postUser(
			@RequestBody UserWrapper user
	) {
		UserEntity userEntity = userRepo.findByGoogleEmail(user.getGoogleEmail());

		if (userEntity == null) {
			userEntity = UserEntity.from(user);
			userRepo.save(userEntity);
			userEntity = userRepo.findByGoogleEmail(user.getGoogleEmail());

			// create folder
			String path = FileUtils.mailPath(imagePath, user.getGoogleEmail());
			try {
				Files.createDirectory(Paths.get(path));
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}
		}

		return UserWrapper.from(userEntity);
	}

	/**
	 * @param id the id of the USER for which to retrieve the COLLECTIONS
	 * @return a list of all COLLECTIONS for the given USER
	 */
	@GetMapping(value = "/{id}/show")
	public @ResponseBody
	Collection<SetWrapper> getCollections(
			@PathVariable("id") long id
	) {
		Collection<CollectionEntity> entityList = collectionRepo.findByUserId(id);

		Collection<SetWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(
				new SetWrapper(entity,
						suiteRepo.countByCollectionsId(entity.getId()))));

		return wrappedList;
	}

	/**
	 * @param id the id of the USER for which to retrieve the COLLECTIONS
	 */
	@PostMapping(value = "/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Status postInCollection(
			@PathVariable("id") long id,
			@RequestBody long suiteId
	) {
		CollectionEntity collection = collectionRepo.findByUserId(id).get(0);

		List<SuiteEntity> suites = collection.getSuites()
				.stream()
				.filter(suite -> suite.getId() == suiteId)
				.collect(Collectors.toList());

		if (suites.size() > 0)
			return Status.ERROR.setMessage("Suite Already in Collection: " + suites);
		else {
			SuiteEntity suite = suiteRepo.findOne(suiteId);
			suite.getCollections().add(collection);

			suite = suiteRepo.save(suite);
			return Status.SUCCESS.setMessage("Added suite: " + suite);
		}
	}

	/**
	 * @param id the id of the USER for which to retrieve the COLLECTIONS
	 * @return a list of all SUITES for the given USER
	 */
	@GetMapping(value = "/{id}/show/all")
	public @ResponseBody
	Collection<SuiteWrapper> getSuites(
			@PathVariable("id") long id
	) {
		Collection<SuiteEntity> entityList = userService.findSuitesByUserId(id);

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(
				new SuiteWrapper(entity,
						questionRepo.countBySuiteId(entity.getId()))));

		return wrappedList;
	}


	@GetMapping(value = "/{id}/show/all/private")
	public @ResponseBody
	Collection<SuiteWrapper> getSuitesEditable(
			@PathVariable("id") long id
	) {
		Collection<SuiteEntity> entityList = userService.findPrivateSuitesByUserId(id);

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(
				new SuiteWrapper(entity,
						questionRepo.countBySuiteId(entity.getId()))));

		return wrappedList;
	}

	// ----------------------------------------------------------------------------
	// region STATS OLD - to work with V1

	@Autowired
	StatsService statsService;

	@Autowired
	CategoryRepo categoryRepo;

	/**
	 * @param id the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/stats")
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStats(
			@PathVariable("id") long id
	) {
		return statsService.findStatsByUserId(id);
	}

	/**
	 * @param id   the id of the USER for which to retrieve the STATS
	 * @param page the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/stats",
			params = {"page"})
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStatsPage(
			@PathVariable("id") long id,
			@RequestParam("page") int page
	) {
		return statsService.findStatsPageByUserId(id, page);
	}

	/**
	 * @param id the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/stats/less")
	public @ResponseBody
	StatsWrapper<StatsQuestionSimpleWrapper> getSimpleStats(
			@PathVariable("id") long id
	) {
		return statsService.findSimpleStatsByUserId(id);
	}

	/**
	 * @param id   the id of the USER for which to retrieve the STATS
	 * @param page the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/stats/less",
			params = {"page"})
	public @ResponseBody
	StatsWrapper<StatsQuestionSimpleWrapper> getSimpleStatsPage(
			@PathVariable("id") long id,
			@RequestParam("page") int page
	) {
		return statsService.findSimpleStatsPageByUserId(id, page);
	}

	@PostMapping(value = "/{id}/stats",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	void postStats(
			@PathVariable("id") long id,
			@RequestBody StatsPostSimpleWrapper stats
	) {
		statsService.addStats(id, stats);
	}

	// ----------------------------------------------------------------------------
	// DATABASE STATS
	// ----------------------------------------------------------------------------

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/db")
	public @ResponseBody
	DatabaseStatsWrapper getDatabaseStats(
			@PathVariable("id") long userId
	) {
		DatabaseStatsWrapper result = new DatabaseStatsWrapper();

		result.setNoPublicSuites(suiteRepo.countPublic());
		result.setNoPulicCategories(categoryRepo.countPublic());

		result.setNoSuites(userService.countSuitesByUserId(userId));
		result.setNoQuestions(userService.countQuestionsByUserId(userId));

		return result;
	}

	// endregion
	// ----------------------------------------------------------------------------
}
