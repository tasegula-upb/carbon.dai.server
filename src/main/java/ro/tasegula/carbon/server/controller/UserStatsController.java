package ro.tasegula.carbon.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tasegula.carbon.server.dao.CategoryRepo;
import ro.tasegula.carbon.server.dao.SuiteRepo;
import ro.tasegula.carbon.server.model.wrappers.*;
import ro.tasegula.carbon.server.service.StatsService;
import ro.tasegula.carbon.server.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/stats/user")
public class UserStatsController {

	@Autowired
	UserService userService;

	@Autowired
	StatsService statsService;

	@Autowired
	CategoryRepo categoryRepo;

	@Autowired
	SuiteRepo suiteRepo;

	/**
	 * @param id the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStats(
			@PathVariable("id") long id
	) {
		return statsService.findStatsByUserId(id);
	}

	/**
	 * @param id   the id of the USER for which to retrieve the STATS
	 * @param page the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}",
			params = {"page"})
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStatsPage(
			@PathVariable("id") long id,
			@RequestParam("page") int page
	) {
		return statsService.findStatsPageByUserId(id, page);
	}

	/**
	 * @param id      the id of the USER for which to retrieve the STATS
	 * @param suiteId the suiteId for which stats are to be retrieved
	 * @return a list of all STATS for the given USER and SUITE
	 */
	@GetMapping(value = "/{id}",
			params = {"suite"})
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStatsForSuite(
			@PathVariable("id") long id,
			@RequestParam("suite") long suiteId
	) {
		StatsWrapper<StatsQuestionWrapper> result = statsService.findStatsByUserId(id);
		List<StatsQuestionWrapper> allStats = result.getStats();

		List<StatsQuestionWrapper> filteredStats =
				allStats.stream()
						.filter(stat -> stat.getSuiteId() == suiteId)
						.collect(Collectors.toList());

		result.setStats(filteredStats);

		return result;
	}

	/**
	 * @param id       the id of the USER for which to retrieve the STATS
	 * @param suiteId the suiteId for which stats are to be retrieved
	 * @return a list of all STATS for the given USER and SUITE
	 */
	@GetMapping(value = "/{id}/less",
			params = {"suite"})
	public @ResponseBody
	StatsWrapper<StatsQuestionSimpleWrapper> getSimpleStatsForSuite(
			@PathVariable("id") long id,
			@RequestParam("suite") long suiteId
			) {
		StatsWrapper<StatsQuestionSimpleWrapper> result = statsService.findSimpleStatsByUserId(id);
		List<StatsQuestionSimpleWrapper> allStats = result.getStats();

		List<StatsQuestionSimpleWrapper> filteredStats =
				allStats.stream()
						.filter(stat -> stat.getSuiteId() == suiteId)
						.collect(Collectors.toList());

		result.setStats(filteredStats);

		return result;
	}

	// region LESS
	/**
	 * @param id the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/less")
	public @ResponseBody
	StatsWrapper<StatsQuestionSimpleWrapper> getSimpleStats(
			@PathVariable("id") long id
	) {
		return statsService.findSimpleStatsByUserId(id);
	}

	/**
	 * @param id   the id of the USER for which to retrieve the STATS
	 * @param page the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/less",
			params = {"page"})
	public @ResponseBody
	StatsWrapper<StatsQuestionSimpleWrapper> getSimpleStatsPage(
			@PathVariable("id") long id,
			@RequestParam("page") int page
	) {
		return statsService.findSimpleStatsPageByUserId(id, page);
	}
	// endregion

	// region POST STATS
	@PostMapping(value = "/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	void postStats(
			@PathVariable("id") long id,
			@RequestBody StatsPostSimpleWrapper stats
	) {
		statsService.addStats(id, stats);
	}
	// endregion

	// ----------------------------------------------------------------------------
	// DATABASE STATS
	// ----------------------------------------------------------------------------

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(value = "/{id}/db")
	public @ResponseBody
	DatabaseStatsWrapper getDatabaseStats(
			@PathVariable("id") long userId
	) {
		DatabaseStatsWrapper result = new DatabaseStatsWrapper();

		result.setNoPublicSuites(suiteRepo.countPublic());
		result.setNoPulicCategories(categoryRepo.countPublic());

		result.setNoSuites(userService.countSuitesByUserId(userId));
		result.setNoQuestions(userService.countQuestionsByUserId(userId));

		return result;
	}
}
