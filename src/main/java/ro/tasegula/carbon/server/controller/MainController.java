package ro.tasegula.carbon.server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class MainController {

	@Value("${image.path}")
	private String imagePath;

	@GetMapping()
	public @ResponseBody
	String getHelloWorld() {
		return "Hello World!\n" +
				"IMAGE FOLDER: " + imagePath + "\n";
	}
}

