package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.tasegula.carbon.server.dao.CollectionRepo;
import ro.tasegula.carbon.server.dao.SuiteRepo;
import ro.tasegula.carbon.server.model.CollectionEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.model.wrappers.SetWrapper;
import ro.tasegula.carbon.server.model.wrappers.SuiteWrapper;

import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/collection")
public class CollectionController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(CollectionController.class);

	@Autowired
	CollectionRepo collectionRepo;

	@Autowired
	SuiteRepo suiteRepo;

	/**
	 * @param id the id of the COLLECTION
	 * @return the COLLECTION from the DB
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	SetWrapper getCollection(
			@PathVariable("id") long id
	) {
		CollectionEntity response = collectionRepo.findOne(id);
		int noSuites = suiteRepo.countByCollectionsId(id);
		return new SetWrapper(response, noSuites);
	}

	/**
	 * @param id the id of the COLLECTION for which to retrieve the SUITES
	 * @return a list of all SUITES in the given COLLECTION
	 */
	@GetMapping(value = "/{id}/show")
	public @ResponseBody
	Collection<SuiteWrapper> getSuites(
			@PathVariable("id") long id
	) {
		Collection<SuiteEntity> entityList = suiteRepo.findByCollectionsId(id);

		Collection<SuiteWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new SuiteWrapper(entity)));

		return wrappedList;
	}

	/**
	 * @param id the id of the COLLECTION for which to retrieve the SUITES
	 * @return a list of ids for all SUITES in the given COLLECTION
	 */
	@GetMapping(value = "/{id}/show/less")
	public @ResponseBody
	Collection<Long> getSuitesId(
			@PathVariable("id") long id
	) {
		Collection<Long> idList = suiteRepo.findIdsByCollectionsId(id);
		return idList;
	}
}
