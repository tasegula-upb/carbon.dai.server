/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tasegula.carbon.server.model.wrappers.StatsPostWrapper;
import ro.tasegula.carbon.server.model.wrappers.StatsQuestionWrapper;
import ro.tasegula.carbon.server.model.wrappers.StatsWrapper;
import ro.tasegula.carbon.server.service.StatsService;

@Controller
@RequestMapping("/stats")
public class StatsController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(StatsController.class);

	@Autowired
	StatsService statsRepo;

	// ----------------------------------------------------------------------------
	// GET
	// ----------------------------------------------------------------------------

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(params = {"userId"})
	public @ResponseBody
	StatsWrapper getSimpleStatsForUser(
			@RequestParam(value = "userId") long userId
	) {
		return statsRepo.findStatsByUserId(userId);
	}

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @param page   the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(params = {"userId", "page"})
	public @ResponseBody
	StatsWrapper<StatsQuestionWrapper> getStatsForUser(
			@RequestParam(value = "userId") long userId,
			@RequestParam(value = "page") int page
	) {
		return statsRepo.findStatsPageByUserId(userId, page);
	}

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @param less   type of showing
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(params = {"userId", "less"})
	public @ResponseBody
	StatsWrapper getSimpleStatsForUser(
			@RequestParam(value = "userId") long userId,
			@RequestParam(value = "less", required = false, defaultValue = "false") boolean less
	) {
		if (less) return statsRepo.findSimpleStatsByUserId(userId);
		else return statsRepo.findStatsByUserId(userId);
	}

	/**
	 * @param userId the id of the USER for which to retrieve the STATS
	 * @param less   type of showing
	 * @param page   the page to be retrieved
	 * @return a list of all STATS for the given USER
	 */
	@GetMapping(params = {"userId", "less", "page"})
	public @ResponseBody
	StatsWrapper getSimpleStatsForUser(
			@RequestParam(value = "userId") long userId,
			@RequestParam(value = "less", required = false, defaultValue = "false") boolean less,
			@RequestParam(value = "page") int page
	) {
		if (less) return statsRepo.findSimpleStatsPageByUserId(userId, page);
		else return statsRepo.findStatsPageByUserId(userId, page);
	}

	/**
	 * @param id the id of the STATS to be retrieved
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	StatsQuestionWrapper getStats(
			@PathVariable("id") long id
	) {
		return statsRepo.findById(id);
	}

	// ----------------------------------------------------------------------------
	// POST
	// ----------------------------------------------------------------------------

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public void postStats(
			@RequestBody StatsPostWrapper stats
	) {
		statsRepo.addStats(stats);
	}
}
