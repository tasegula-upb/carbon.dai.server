/**
 * Project Carbon: created by Tase Gula.
 * <p>
 * This file is part of the project's server.
 */
package ro.tasegula.carbon.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.tasegula.carbon.server.dao.*;
import ro.tasegula.carbon.server.model.CollectionEntity;
import ro.tasegula.carbon.server.model.ImageEntity;
import ro.tasegula.carbon.server.model.QuestionEntity;
import ro.tasegula.carbon.server.model.SuiteEntity;
import ro.tasegula.carbon.server.model.wrappers.QuestionWrapper;
import ro.tasegula.carbon.server.model.wrappers.SuiteWrapper;
import ro.tasegula.carbon.server.util.DB.DbPages;
import ro.tasegula.carbon.server.util.Defaults;
import ro.tasegula.carbon.server.util.FileUtils;
import ro.tasegula.carbon.server.util.Utils;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/suite")
public class ViewSuiteController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewSuiteController.class);

	@Value("${image.path}")
	private String imagePath;

	@Autowired
	UserRepo userRepo;

	@Autowired
	SuiteRepo suiteRepo;

	@Autowired
	QuestionRepo questionRepo;

	@Autowired
	ImageRepo imageRepo;

	@Autowired
	CollectionRepo collectionRepo;

	// ----------------------------------------------------------------------------
	// GET
	// ----------------------------------------------------------------------------

	/**
	 * @param id the id of the SUITE
	 * @return the SUITE from the DB
	 */
	@GetMapping(value = "/{id}")
	public @ResponseBody
	SuiteWrapper getSuite(
			@PathVariable("id") long id
	) {
		SuiteWrapper response = new SuiteWrapper(suiteRepo.findOne(id));
		response.setNoQuestions(questionRepo.countBySuiteId(id));
		return response;
	}

	/**
	 * @param id the id of the SUITE for which to retrieve the QUESTIONS
	 * @return a list of all QUESTIONS in the given SUITE
	 */
	@GetMapping(value = "/{id}/show")
	public @ResponseBody
	Collection<QuestionWrapper> getQuestions(
			@PathVariable("id") long id
	) {
		Collection<QuestionEntity> entityList;
		entityList = questionRepo.findBySuiteId(id);

		Collection<QuestionWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new QuestionWrapper(entity)));

		return wrappedList;
	}

	/**
	 * @param id the id of the SUITE for which to retrieve the QUESTIONS
	 * @return a list of ids for all QUESTIONS in the given SUITE
	 */
	@GetMapping(value = "/{id}/show/less")
	public @ResponseBody
	Collection<Long> getQuestionsId(
			@PathVariable("id") long id
	) {
		Collection<Long> idList = questionRepo.findIdsBySuiteId(id);
		return idList;
	}

	/**
	 * @param id  the id of the SUITE for which to retrieve the QUESTIONS
	 * @param max the max number of QUESTIONS to be retrieved
	 * @return max number of QUESTIONS in the given SUITE
	 */
	@GetMapping(value = "/{id}/show",
			params = {"max"})
	public @ResponseBody
	Collection<QuestionWrapper> getMaxQuestions(
			@PathVariable("id") long id,
			@RequestParam(value = "max") int max
	) {
		Collection<QuestionEntity> entityList;
		entityList = questionRepo.findFirstBySuitesId(id, max);

		Collection<QuestionWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> wrappedList.add(new QuestionWrapper(entity)));

		return wrappedList;
	}

	/**
	 * @param id   the id of the SUITE for which to retrieve the QUESTIONS
	 * @param page the page of QUESTIONS to be retrieved
	 * @return a list with the QUESTIONS in the given page of the SUITE
	 */
	@GetMapping(value = "/{id}/show",
			params = {"page"})
	public @ResponseBody
	Collection<QuestionWrapper> getPageQuestions(
			@PathVariable("id") long id,
			@RequestParam(value = "page") int page
	) {
		Collection<QuestionEntity> entityList;
		entityList = questionRepo.findPageBySuiteId(id, DbPages.getPage(page));
		List<Long> IDs = new ArrayList<>();

		Collection<QuestionWrapper> wrappedList = new ArrayList<>();
		entityList.forEach(entity -> {
			if (!IDs.contains(entity.getId())) {
				IDs.add(entity.getId());
				wrappedList.add(new QuestionWrapper(entity));
			}
		});

		return wrappedList;
	}

	// ----------------------------------------------------------------------------
	// POST
	// ----------------------------------------------------------------------------

	/**
	 * Adds a SUITE in DB
	 *
	 * @param suite  the suite to be added
	 * @param userId the user id for which the suite is added
	 * @return the object added in DB
	 */
	@PostMapping(params = {"userId"},
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	SuiteWrapper postSuite(
			@RequestBody SuiteWrapper suite,
			@RequestParam(value = "userId") long userId
	) {
		SuiteEntity entity = SuiteEntity.from(suite);
		ImageEntity image = imageRepo.findOne((long) Defaults.Suite.IMAGE_ID);

		entity.setImage(image);

		// connect to collection
		CollectionEntity collection = collectionRepo.findByUserId(userId).get(0);

		// TODO: suiteRepo.copyById(entity.getId(), collection.getId());
		entity.getCollections().add(collection);
		entity = suiteRepo.save(entity);

		collection.getSuites().add(entity);
		collectionRepo.save(collection);

		// connect to collection
		return new SuiteWrapper(entity);
	}

	/**
	 * Adds a SUITE in DB
	 *
	 * @param suiteJson suite data in String format
	 * @param file      the image file
	 * @param userId    the user id for which the suite is added
	 * @return the object added in DB
	 */
	@PostMapping(value = "/image",
			params = {"userId"},
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	SuiteWrapper postSuiteWithImage(
			@RequestParam("body") String suiteJson,
			@RequestParam("file") MultipartFile file,
			@RequestParam(value = "userId") long userId
	) {
		String userFolder = FileUtils.userFolder(userRepo.findOne(userId).getGoogleEmail());

		// convert object
		ObjectMapper mapper = new ObjectMapper();
		SuiteWrapper suite;
		try {
			suite = mapper.readValue(suiteJson, SuiteWrapper.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		SuiteEntity entity = SuiteEntity.from(suite);

		// construct image entity
		if (!Utils.isStringEmtpy(suite.getImageUri())) {
			Timestamp time = new Timestamp((new Date()).getTime());

			String path = FileUtils.imagePath(imagePath, userFolder);
			String name = userFolder + "_" + time.getTime() + "." + FileUtils.getExtension(file.getOriginalFilename());
			long size = FileUtils.saveFile(file, path, name);

			ImageEntity image = constructImage(userFolder, name, size);
			entity.setImage(image);
		}

		// connect to collection
		CollectionEntity collection = collectionRepo.findByUserId(userId).get(0);

//		TODO: suiteRepo.copyById(entity.getId(), collection.getId());
		entity.getCollections().add(collection);
		entity = suiteRepo.save(entity);

		collection.getSuites().add(entity);
		collectionRepo.save(collection);

		return new SuiteWrapper(entity);
	}

	// ----------------------------------------------------------------------------
	// HELPERS
	// ----------------------------------------------------------------------------

	private ImageEntity constructImage(String filepath, String filename, long size) {
		ImageEntity image = new ImageEntity();

		image.setFilepath("/users/" + filepath);
		image.setFilename(filename);
		image.setSize(size);

		return imageRepo.save(image);
	}

}
