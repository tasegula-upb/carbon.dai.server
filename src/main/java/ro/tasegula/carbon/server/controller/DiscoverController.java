package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.tasegula.carbon.server.dao.CategoryRepo;
import ro.tasegula.carbon.server.model.wrappers.SetWrapper;

import java.util.List;

@Controller
@RequestMapping("/discover")
public class DiscoverController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(DiscoverController.class);

	@Autowired
	CategoryRepo categoryRepo;

	@GetMapping()
	public @ResponseBody
	List<SetWrapper> getCategories() {
		List<SetWrapper> response = categoryRepo.findAllNames();
		return response;
	}
}
