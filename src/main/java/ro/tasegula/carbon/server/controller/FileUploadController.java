package ro.tasegula.carbon.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.tasegula.carbon.server.util.FileUtils;

@Controller
@RequestMapping("/upload")
public class FileUploadController {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

	@GetMapping()
	public @ResponseBody
	String provideUploadInfo() {
		return "You can upload a file by posting to this same URL.";
	}

	@Value("${image.path}")
	private String imagePath;

	@PostMapping()
	public @ResponseBody
	long handleFileUpload(
			@RequestParam("file") MultipartFile file) {
		return FileUtils.saveFile(file, imagePath, file.getOriginalFilename());
	}

	@PostMapping(value = "/new")
	public @ResponseBody
	long handleFileUpload(
			@PathVariable("path") String path,
			@PathVariable("name") String name,
			@RequestParam("file") MultipartFile file) {
		return FileUtils.saveFile(file, imagePath + "/" + path, name);
	}
}
